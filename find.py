# Author        : Javier Blanco 
# Created       : 27 May 2021
# Last Modified : 28 May 2021
# Version       : 1.0.2
# Creation      : 1.0.0 - Creation of first script
# Modifications : 1.0.1 - Completing tests to check full functionallity
# Modifications : 1.0.2 - Tidy up comments, syntax and demo
# Description   : Tool suited to evaluate a float value as possible 
#           ouput of an increasing function, and inside a range of 
#           values. To do that find method inside FindTool class must 
#           be executed.
# Use           : In Python Script import FindTool, then import or create 
#           function to evaluate and then call find method with suitable 
#           arguments. In File "sampleTest.py" there's a functional example with instructions
#           
#


class FindTool:
    """" Class that holds the methods in use"""

    @staticmethod
    def find(evaluated_function, target_result, first_value, last_value):
        """ description: executes a binary search to test sample values
            arguments:
                - evaluated_function (function): function to be evaluated,
                        Inputs: integer
                        Returns: float
                - target_result(float): value to be checked in evaluated_function
                - first_value(int>0): beginning of range of possible results
                - last_value(int>0): ending of range of possible results
            returned value:
                - (integer in [first_value, last_value]) - in case this value
                        evaluated_function(integer) = target_result
                - '-1' - in case no value in range evaluated in target_result
        """
        # Check if Ranges are valid
        if (first_value < 0 or not (isinstance(first_value, int))) or (
                last_value < 0 or not (isinstance(first_value, int))):
            return -1

        # Correct Ranges in order to put larger at end if needed
        if last_value < first_value:
            temp = first_value
            first_value = last_value
            last_value = temp

        # Binary search checking values and returning response
        while last_value > first_value + 1:
            pointerValue = (last_value - first_value) // 2 + first_value

            pointerResult = evaluated_function(pointerValue)
            if pointerResult > target_result:
                last_value = pointerValue
            elif pointerResult < target_result:
                first_value = pointerValue
            else:
                return pointerValue
        if evaluated_function(last_value) == target_result:
            return last_value
        elif evaluated_function(first_value) == target_result:
            return first_value
        return -1
