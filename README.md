# Find Tool

Tool suited to evaluate a float value as possible ouput of an increasing function, and inside a range of values. To do that find method inside FindTool class must 
be executed.

Code created in Python, this is not a stand alone tool. It's implemented as a function inside a class(FindTool). This has to be used inside a software that call's this function. In SampleTest.py there's a functional example.



## File Description
<hr/>

* README.md  - Description and details to execute the find tool.
* INSTRUCTIONS.md  - Description and details of the programming assignment.
* find.py  - Python Script that contains the required functionallity. 
* test_find.py  - Python script with UnitTests file to develop and check functionallity.
* sampleTest.py  - Python Script to show usage of the tool.

