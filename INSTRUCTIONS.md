# Programming Assignment


Carefully read the instructions. You are asked to implement the following functionality, preferably in Python:

Given:

* an increasing function f of an integer argument that evaluates to a floating point value,
* a range [a,b] of non-negative integers, and
* an arbitrary floating point number y,
write a function that
* returns n if for any integer n with a<=n<=b we have that f(n)=y
* returns -1 if there is no such n with a<=n<=b and f(n)=y

In python, the function signature could be

    def find(f, y, a, b):
        pass

In C++ it could be

    long find(double(*f)(long), double y, long a, long b);

* You only have to consider exact equality of the numbers, differences due to machine precision etc. can be disregarded.

* A solution that works for functions that are slow to evaluate, or that are able to perform tghe search on very large input ranges[a,b] is highly 
favored. An example of such a function and its use is

    ```python
    from mpmath import zetazero # need mpmath installed

    def f(n):
        return 0. if n<=0 else float(zetazero(n).imag)

    y = f(123456789) # value in the range
    z = 12345.6789   # value not in the range

    // This sould output 123456789 in one or two minutes
    print(find(f, y, 0, 10000000000))

    // This should output -1 in one or two minutes
    print(find(f, y+1e-8, 0, 10000000000))
    ```

* Don't use libraries for the implementation of the core functionality

This is supposed to be enterprise-grade software:

* Include usage of notes within the code file itself, in Python in the form of a docstring, in Java or C++ you include a block delimited by

        /** ... */

These notes should be sufficient for someone to underestand how they can use your function in their own code. No need to write separate documentation.

* Include Comments. These are for other developers to understand your implementation.
* Develop unit tests. Make sure to cover some relevant use cases.
* If this is not obvious, add instructions on how to compile (if applicable) and execute the tests.

If you want to comment on any design decisions or doubts about the assignment and how you deal with them, you can write a short accompanying note.
We don't expect more than what can be done in a few hours, but you don't have to rush it either.