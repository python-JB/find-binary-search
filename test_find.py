# Script Name   : test_find.py
# Author        : Javier Blanco 
# Created       : 27 May 2021
# Last Modified	: 28 May 2021
# Version       : 1.0.2
# Creation      : 1.0.0 - Creation of first test
# Modifications : 1.0.1 - Completing tests to check full functionallity
# Modifications	: 1.0.1 - Tidy up comments and syntax
# Use           : in shell run
#                        python test_find.py
#           
#

import unittest

from find import FindTool
from mpmath import zetazero


class TestFind(unittest.TestCase):

    # before all tests
    @classmethod
    def setUpClass(cls):
        print("UnitTest Battery is starting!!")

    # Set of functions to evaluate tests
    def two_times(self, n):
        return 0. if n <= 0 else float(2 * n)

    def zetazeroing(self, number):
        return 0. if number <= 0 else float(zetazero(number).imag)

    # test to check functionality   
    def test_inc(self):
        self.assertEqual(FindTool.find(self.two_times, 4, 0, 10), 2)

    def test_proposed(self):
        y = self.zetazeroing(1256)
        self.assertEqual(FindTool.find(self.zetazeroing, y, 0, 10000000000), 1256)

    def test_proposed2fail(self):
        y = self.zetazeroing(123456789)
        self.assertEqual(FindTool.find(self.zetazeroing, y + 1e-8, 0, 10000000000), -1)

    def test_checkResultOnUpperLimit(self):
        y = self.zetazeroing(1000)
        self.assertEqual(FindTool.find(self.zetazeroing, y, 0, 1000), 1000)

    def test_checkResultOnLowerLimit(self):
        y = self.zetazeroing(0)
        self.assertEqual(FindTool.find(self.zetazeroing, y, 0, 1000), 0)

    def test_checkResultOnInvalidRange(self):
        y = self.zetazeroing(123456789)
        self.assertEqual(FindTool.find(self.zetazeroing, y, -10, 1000), -1)

    def test_checkResultOnInvalidRange2(self):
        y = self.zetazeroing(123456789)
        self.assertEqual(FindTool.find(self.zetazeroing, y, 10.5, 1000), -1)

    def test_checkResultOnOnePositionRange(self):
        y = self.zetazeroing(1000)
        self.assertEqual(FindTool.find(self.zetazeroing, y, 1000, 1000), 1000)

    def test_checkResultOnInvertedRange(self):
        y = self.zetazeroing(1000)
        self.assertEqual(FindTool.find(self.zetazeroing, y, 10000, 100), 1000)


if __name__ == '__main__':
    unittest.main()
