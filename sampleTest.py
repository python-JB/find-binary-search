#####################################
# only for validation purposes
#####################################
# execute in shell 
#       python sampleTest.py
#####################################
from find import FindTool


def two_times(n):
    return 0. if n<=0 else float(2*n)

print("\n----------------------------------------------------")
print("Sample execution to show behavior and implementation")
print("----------------------------------------------------")

print("\nExecuting find tool to check if any number bettwen 0 ");
print("and 10000 multiplied by 2 results in 100")
print("COMMAND: FindTool.find(two_times, 100, 0, 10000)")
print("Expected Result: 50")
print("Actual Result: " + str(FindTool.find(two_times,100, 0, 10000)))

print(not(isinstance(-10.5, int)))